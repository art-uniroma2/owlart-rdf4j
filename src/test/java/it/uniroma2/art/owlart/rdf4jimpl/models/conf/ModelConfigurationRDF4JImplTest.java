/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART OWL API.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
 * All Rights Reserved.
 *
 * The ART OWL API were developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART OWL API can be obtained at 
 * http://art.uniroma2.it/owlart
 *
 */

package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

import static org.junit.Assert.*;

import java.util.Collection;

import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.models.conf.ConfParameterNotFoundException;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JPersistentInMemoryModelConfiguration;

import org.junit.Test;

/**
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ModelConfigurationRDF4JImplTest {

	static public final String testRepoFolder = "./src/test/resources/testRepo";
	static final String baseURI = "http://pippo.it";
	static final String resourcesFolder = "./../OWLArtAPI/src/test/resources";
	static final String importsFolder = resourcesFolder + "/imports";

	@Test
	public void testBasicModelConfiguration() throws ConfParameterNotFoundException, UnsupportedModelConfigurationException, UnloadableModelConfigurationException {
		ARTModelFactoryRDF4JImpl modelFactory = new ARTModelFactoryRDF4JImpl();
		RDF4JModelConfiguration modelConf = modelFactory.createModelConfigurationObject(RDF4JPersistentInMemoryModelConfiguration.class);
		System.out.println("Model Configuration for model class: " + modelConf.getClass().getName());

		Collection<String> modelParameters = modelConf.getConfigurationParameters();
		System.out.println("----- Model Parameters -----");
		for (String par : modelParameters) {
			System.out.print(par + " : ");
			System.out.println(modelConf.getParameterDescription(par));
		}
		System.out.println("----------------------------");

		try {
			modelConf.setParameter("directTypeInference", "false");
			modelConf.setParameter("syncDelay", "10000");
		} catch (BadConfigurationException e) {
			e.printStackTrace();
		}

		System.out.println("value for \"directTypeInference\": "
				+ modelConf.getParameterValue("directTypeInference"));

		// default value specified in the class
		assertTrue((Boolean) modelConf.getParameterValue("rdfsInference"));
		//asserted false value
		assertFalse((Boolean) modelConf.getParameterValue("directTypeInference"));
		//asserted 10000 value
		assertEquals((Long) 10000L, modelConf.getParameterValue("syncDelay"));

		System.out.println("value for \"rdfsInference\": " + modelConf.getParameterValue("rdfsInference"));

		System.out.println("value for \"syncDelay\": " + modelConf.getParameterValue("syncDelay"));

	}

}
