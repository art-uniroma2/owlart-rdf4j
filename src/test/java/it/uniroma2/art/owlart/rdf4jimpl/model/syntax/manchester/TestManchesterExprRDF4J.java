package it.uniroma2.art.owlart.rdf4jimpl.model.syntax.manchester;

import java.io.File;
import java.io.IOException;

import org.antlr.runtime.RecognitionException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import it.uniroma2.art.owlart.exceptions.ManchesterParserException;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.model.syntax.manchester.TestManchesterExpr;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;
import it.uniroma2.art.owlart.utilites.refactor.BaseURIRefactorTest;

public class TestManchesterExprRDF4J extends TestManchesterExpr{

	@BeforeClass
	public static void loadRepository() throws Exception {
		TestManchesterExpr.initializeTest(new ARTModelFactoryRDF4JImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			TestManchesterExpr.closeRepository();
			File memStoreFile = new File(TestManchesterExpr.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}
	
}
