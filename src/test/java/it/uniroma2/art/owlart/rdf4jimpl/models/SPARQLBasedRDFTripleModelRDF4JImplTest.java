package it.uniroma2.art.owlart.rdf4jimpl.models;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.SPARQLBasedRDFTripleModelImplTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;

public class SPARQLBasedRDFTripleModelRDF4JImplTest extends SPARQLBasedRDFTripleModelImplTest {

	@BeforeClass
	public static void initialize() {
		SPARQLBasedRDFTripleModelImplTest.initializeModelFactory(new ARTModelFactoryRDF4JImpl());		
	}
	
	
	
}
