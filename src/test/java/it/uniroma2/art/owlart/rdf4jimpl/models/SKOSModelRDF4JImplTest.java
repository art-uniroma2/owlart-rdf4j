package it.uniroma2.art.owlart.rdf4jimpl.models;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.SKOSModelTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;

/**
 * basic Sesame4 implementation for unit tests of OWL ART API
 * 
 * @author Luca Mastrogiovanni <luca.mastrogiovanni@caspur.it>
 * 
 */
public class SKOSModelRDF4JImplTest extends SKOSModelTest {
	
	@BeforeClass
	public static void loadRepository() throws Exception {
		SKOSModelTest.initializeTest(new ARTModelFactoryRDF4JImpl(),false);
	}

	@AfterClass
	public static void classTearDown() {
		try {			
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}

	}

}
