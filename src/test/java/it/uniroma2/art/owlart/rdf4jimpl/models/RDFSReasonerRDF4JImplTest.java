package it.uniroma2.art.owlart.rdf4jimpl.models;

import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.RDFSReasonerTest;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JDirectAccessModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JNonPersistentInMemoryModelConfiguration;

public class RDFSReasonerRDF4JImplTest extends RDFSReasonerTest<RDF4JModelConfiguration> {

	public ARTModelFactoryRDF4JImpl getModelFactory() {
		return new ARTModelFactoryRDF4JImpl();
	}

	public RDF4JModelConfiguration getNonReasoningModelConfiguration(
			ModelFactory<RDF4JModelConfiguration> factImpl) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		RDF4JDirectAccessModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(RDF4JNonPersistentInMemoryModelConfiguration.class);
		modelConf.directTypeInference = false;
		modelConf.rdfsInference = false;
		return modelConf;
	}

	public RDF4JModelConfiguration getReasoningModelConfiguration(
			ModelFactory<RDF4JModelConfiguration> factImpl) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		// default configuration covers RDFS reasoning
		return factImpl.createModelConfigurationObject(RDF4JNonPersistentInMemoryModelConfiguration.class);
	}

}
