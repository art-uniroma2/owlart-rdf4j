package it.uniroma2.art.owlart.rdf4jimpl.models;

import it.uniroma2.art.owlart.models.LinkedDataResolverTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;

import org.junit.BeforeClass;

/**
 * basic Sesame2 implementation for unit tests of OWL ART API
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class LinkedDataResolverRDF4JImplTest extends LinkedDataResolverTest {
	
	@BeforeClass
	public static void loadLinkedDataResolver() throws Exception {
		initializeTest(new ARTModelFactoryRDF4JImpl());
	}
}
