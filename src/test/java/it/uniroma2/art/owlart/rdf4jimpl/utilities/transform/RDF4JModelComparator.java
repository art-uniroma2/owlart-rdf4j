package it.uniroma2.art.owlart.rdf4jimpl.utilities.transform;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.Models;

import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.rdf4jimpl.RDF4JARTResourceFactory;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTStatementRDF4JImpl;
import it.uniroma2.art.owlart.utilites.transform.ModelComparator;

public class RDF4JModelComparator implements ModelComparator {
	private RDF4JARTResourceFactory rdf4j2artFact = new RDF4JARTResourceFactory(
			SimpleValueFactory.getInstance());

	public boolean equals(RDFModel firstModel, RDFModel secondModel) throws Exception {
		ARTStatementIterator it = firstModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY,
				false, NodeFilters.MAINGRAPH);
		Set<Statement> firstSet = new HashSet<Statement>();
		while (it.hasNext()) {
			firstSet.add(((ARTStatementRDF4JImpl) it.next()).getRDF4JStatement());
		}
		it.close();

		it = secondModel.listStatements(NodeFilters.ANY, NodeFilters.ANY, NodeFilters.ANY, false,
				NodeFilters.MAINGRAPH);
		Set<Statement> secondSet = new HashSet<Statement>();
		while (it.hasNext()) {
			secondSet.add(((ARTStatementRDF4JImpl) it.next()).getRDF4JStatement());
		}
		it.close();

		return Models.isomorphic(firstSet, secondSet);
	}

	public boolean equals(Collection<ARTStatement> firstModel, Collection<ARTStatement> secondModel)
			throws Exception {
		List<Statement> sesFirstModel = firstModel.stream().map(rdf4j2artFact::aRTStatement2RDF4JStatement)
				.collect(Collectors.toList());
		List<Statement> sesSecondModel = secondModel.stream().map(rdf4j2artFact::aRTStatement2RDF4JStatement)
				.collect(Collectors.toList());

		return Models.isomorphic(sesFirstModel, sesSecondModel);
	}
}
