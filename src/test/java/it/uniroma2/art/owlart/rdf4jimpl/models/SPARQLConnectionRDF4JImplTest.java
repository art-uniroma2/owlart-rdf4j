package it.uniroma2.art.owlart.rdf4jimpl.models;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.SPARQLConnectionTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;

public class SPARQLConnectionRDF4JImplTest extends SPARQLConnectionTest {

	@BeforeClass
	public static void initialize() {
		SPARQLConnectionTest.initializeModelFactory(new ARTModelFactoryRDF4JImpl());
	}
	
	
	
}
