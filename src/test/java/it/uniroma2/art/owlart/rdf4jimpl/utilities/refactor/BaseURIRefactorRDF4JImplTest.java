package it.uniroma2.art.owlart.rdf4jimpl.utilities.refactor;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;
import it.uniroma2.art.owlart.utilites.refactor.BaseURIRefactorTest;

public class BaseURIRefactorRDF4JImplTest extends BaseURIRefactorTest{

	@BeforeClass
	public static void loadRepository() throws Exception {
		BaseURIRefactorTest.initializeTest(new ARTModelFactoryRDF4JImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseURIRefactorTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}
}
