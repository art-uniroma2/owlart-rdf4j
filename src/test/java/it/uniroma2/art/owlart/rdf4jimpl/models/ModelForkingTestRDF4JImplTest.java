package it.uniroma2.art.owlart.rdf4jimpl.models;

import org.junit.BeforeClass;

import it.uniroma2.art.owlart.models.ModelForkingTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;

public class ModelForkingTestRDF4JImplTest extends ModelForkingTest {

	@BeforeClass
	public static void initialize() throws Exception {
		initializeTest(new ARTModelFactoryRDF4JImpl());
	}
	
}
