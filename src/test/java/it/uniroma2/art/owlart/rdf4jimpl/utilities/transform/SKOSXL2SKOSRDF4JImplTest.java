package it.uniroma2.art.owlart.rdf4jimpl.utilities.transform;

import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JNonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.utilites.transform.SKOSXL2SKOSConverterTest;

import org.junit.Before;

public class SKOSXL2SKOSRDF4JImplTest extends SKOSXL2SKOSConverterTest {
	@Before
	public void initializeTest() throws Exception {
		ARTModelFactoryRDF4JImpl factImpl = new ARTModelFactoryRDF4JImpl();

		RDF4JNonPersistentInMemoryModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(RDF4JNonPersistentInMemoryModelConfiguration.class);
		modelConf.rdfsInference = true;

		RDF4JNonPersistentInMemoryModelConfiguration modelConf2 = factImpl
				.createModelConfigurationObject(RDF4JNonPersistentInMemoryModelConfiguration.class);
		modelConf2.rdfsInference = false;

		RDF4JNonPersistentInMemoryModelConfiguration modelConf3 = factImpl
				.createModelConfigurationObject(RDF4JNonPersistentInMemoryModelConfiguration.class);
		modelConf3.rdfsInference = false;

		initializeTest(new RDF4JModelComparator(), factImpl, modelConf, modelConf2, modelConf3);
	}
}
