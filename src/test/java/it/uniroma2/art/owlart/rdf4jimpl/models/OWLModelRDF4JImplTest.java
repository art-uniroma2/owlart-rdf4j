package it.uniroma2.art.owlart.rdf4jimpl.models;

import java.io.File;
import java.util.Collection;

import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.BaseRDFModelTest;
import it.uniroma2.art.owlart.models.OWLModelTest;
import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.vocabulary.SESAME;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * basic Sesame4 implementation for unit tests of OWL ART API
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class OWLModelRDF4JImplTest extends OWLModelTest {

	@BeforeClass
	public static void loadRepository() throws Exception {
		OWLModelTest.initializeTest(new ARTModelFactoryRDF4JImpl());
	}

	@AfterClass
	public static void classTearDown() {
		try {
			BaseRDFModelTest.closeRepository();
			File memStoreFile = new File(BaseRDFModelTest.testRepoFolder, "memorystore.data");
			boolean deleted = memStoreFile.delete();
			if (deleted)
				System.out.println("repository file deleted");
			else
				System.err.println("failed to delete repository file");
			System.out.println("-- test teared down --");
		} catch (Exception e) {
			System.err.println("failed to close the repository");
		}
	}

	protected void addTripleStoreSpecificNamedResources(Collection<ARTURIResource> expectedNamedResources) {
		expectedNamedResources.add(SESAME.Res.DIRECTSUBCLASSOF);
		expectedNamedResources.add(SESAME.Res.DIRECTSUBPROPERTYOF);
		expectedNamedResources.add(SESAME.Res.DIRECTTYPE);
	}
}
