/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.query;

import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.query.Binding;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTNodeRDF4JImpl;

public class BindingRDF4JImpl implements Binding {

	org.eclipse.rdf4j.query.Binding binding;
	
	BindingRDF4JImpl(org.eclipse.rdf4j.query.Binding binding) {
		this.binding = binding;
	}
	
	public ARTNode getBoundValue() {
		return new ARTNodeRDF4JImpl(binding.getValue());
	}

	public String getName() {
		return binding.getName();
	}

	public String toString() {
		return binding.toString();
	}

}
