/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.model;

import org.eclipse.rdf4j.model.URI;

import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTURIResourceRDF4JImpl extends ARTResourceRDF4JImpl implements ARTURIResource {

	public ARTURIResourceRDF4JImpl(URI res) {
		super(res);
	}
	
	public String getLocalName() {
		return ((URI)node).getLocalName();
	}

	public String getNamespace() {
		return ((URI)node).getNamespace();
	}

	public String getURI() {
		return ((URI)node).stringValue();
	}
	
	/**
	 * convenience method for extracting encapsulated RDF4J {@link URI}
	 * 
	 * @return
	 */
	public URI getRDF4JURI() {
		return (URI)node;
	}
	

    
}
