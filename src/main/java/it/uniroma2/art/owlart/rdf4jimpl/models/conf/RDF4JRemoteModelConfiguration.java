package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;
import it.uniroma2.art.owlart.models.conf.RemoteModelConfigurationImpl;
import it.uniroma2.art.owlart.models.conf.RequiredConfigurationParameter;

public class RDF4JRemoteModelConfiguration extends RemoteModelConfigurationImpl implements
		RDF4JModelConfiguration, PersistenceModelConfiguration {

	@ModelConfigurationParameter(description = "id of the RDFs4J repository to be accessed")
	@RequiredConfigurationParameter
	public String repositoryId;

	@ModelConfigurationParameter(description = "set to 'false' if the inferred null context does not contain triples from other named graphs (e.g. as for OWLIM)")
	public boolean inferredNullContextContainsAllTriples = true;

	public RDF4JRemoteModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "remote access";
	}

	public boolean isPersistent() {
		return true;
	}
}
