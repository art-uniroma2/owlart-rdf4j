package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

public class RDF4JNonPersistentInMemoryModelConfiguration extends RDF4JDirectAccessModelConfiguration
		implements RDF4JInMemoryModelConfiguration {

	public RDF4JNonPersistentInMemoryModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "in memory / no persist";
	}

}
