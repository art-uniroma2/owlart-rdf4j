/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */


package it.uniroma2.art.owlart.rdf4jimpl.vocabulary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;

/**
 * This class provides static references to SESAME vocabulary, containing definitions for properties
 * expressing direct subproperty/subclass/type relationships which can be inferred through reasoning
 * 
 * @author Armando Stellato
 * 
 */
public class SESAME {

	/** http://www.openrdf.org/schema/sesame# **/
	public static final String NAMESPACE = "http://www.openrdf.org/schema/sesame#";

	/** http://www.openrdf.org/schema/sesame#directSubClassOf **/
	public static final String DIRECTSUBCLASSOF = NAMESPACE + "directSubClassOf";

	/** http://www.openrdf.org/schema/sesame#directSubPropertyOf **/
	public static final String DIRECTSUBPROPERTYOF = NAMESPACE + "directSubPropertyOf";

	/** http://www.openrdf.org/schema/sesame#directType **/
	public static final String DIRECTTYPE = NAMESPACE + "directType";

	public static class Res {
		public static ARTURIResource DIRECTSUBCLASSOF;
		public static ARTURIResource DIRECTSUBPROPERTYOF;
		public static ARTURIResource DIRECTTYPE;

		public static void initialize(RDFModel repo) throws VocabularyInitializationException {

			DIRECTSUBCLASSOF = repo.createURIResource(SESAME.DIRECTSUBCLASSOF);
			DIRECTSUBPROPERTYOF = repo.createURIResource(SESAME.DIRECTSUBPROPERTYOF);
			DIRECTTYPE = repo.createURIResource(SESAME.DIRECTTYPE);

			if (DIRECTSUBCLASSOF == null || DIRECTSUBPROPERTYOF == null || DIRECTTYPE == null)

				throw new VocabularyInitializationException(
						"Problems occurred in initializing the SESAME vocabulary");

		}

	}

}
