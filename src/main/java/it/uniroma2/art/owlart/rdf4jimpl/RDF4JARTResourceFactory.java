/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.URI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTBNodeRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTLiteralRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTNodeRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTResourceRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTStatementRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTURIResourceRDF4JImpl;

/**
 * this class provides convenience methods for bidirectional conversion of RDF4J RDF types and OWL ART RDF
 * types
 * 
 * @author Armando Stellato
 * 
 */
public class RDF4JARTResourceFactory {

	protected ValueFactory vFact;

	public RDF4JARTResourceFactory(ValueFactory vFact) {
		this.vFact = vFact;
	}

	// *********************
	// *** ART 2 RDF4J ****
	// *********************

	/**
	 * converts an OWL ART {@link ARTStatement} to a RDF4J {@link Statement}
	 * 
	 * @param artStat
	 * @return
	 */
	public Statement aRTStatement2RDF4JStatement(ARTStatement artStat) {
		if (artStat == null)
			return null;
		if (artStat instanceof ARTStatementRDF4JImpl)
			return ((ARTStatementRDF4JImpl) artStat).getRDF4JStatement();
		else {
			Statement stat = ((ARTStatementRDF4JImpl) artStat).getRDF4JStatement();
			return vFact.createStatement(stat.getSubject(), stat.getPredicate(), stat.getObject());
		}			
	}

	/**
	 * converts an OWL ART {@link ARTURIResource} to a RDF4J {@link URI}
	 * 
	 * @param sTres
	 * @return
	 */
	public URI aRTURIResource2RDF4JURI(ARTURIResource sTres) {
		if (sTres == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (sTres == NodeFilters.ANY)
			return null;
		if (sTres instanceof ARTURIResourceRDF4JImpl)
			return ((ARTURIResourceRDF4JImpl) sTres).getRDF4JURI();
		return vFact.createURI(sTres.getURI());
	}

	/**
	 * converts an OWL ART {@link ARTResource} to a RDF4J {@link Resource}
	 * 
	 * @param sTres
	 * @return
	 */
	public Resource aRTResource2RDF4JResource(ARTResource sTres) {
		if (sTres == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (sTres == NodeFilters.ANY)
			return null;
		if (sTres instanceof ARTResourceRDF4JImpl)
			return ((ARTResourceRDF4JImpl) sTres).getRDF4JResource();

		if (sTres.isBlank()) {
			return vFact.createBNode(sTres.asBNode().getID());
		}

		return vFact.createURI(sTres.asURIResource().getURI());
	}

	/**
	 * converts an OWL ART {@link ARTNode} to a RDF4J {@link Value}
	 * 
	 * @param sTNode
	 * @return
	 */
	public Value aRTNode2RDF4JValue(ARTNode sTNode) {
		if (sTNode == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (sTNode == NodeFilters.ANY)
			return null;
		if (sTNode instanceof ARTNodeRDF4JImpl)
			return ((ARTNodeRDF4JImpl) sTNode).getRDF4JValue();

		if (sTNode.isBlank()) {
			return vFact.createBNode(sTNode.asBNode().getID());
		}
		if (sTNode.isLiteral()) {
			ARTLiteral lit = sTNode.asLiteral();
			
			String lang = lit.getLanguage();
			
			if (lang != null) {
				return vFact.createLiteral(lit.getLabel(), lang);
			} else {
				ARTURIResource dt = lit.getDatatype();
				if (dt != null)
					return vFact.createLiteral(lit.getLabel(), vFact.createURI(dt.getURI()));
				else
					return vFact.createLiteral(lit.getLabel());
			}
		}
		return vFact.createURI(sTNode.asURIResource().getURI());
	}

	/**
	 * converts an OWL ART {@link ARTLiteral} to a RDF4J {@link Literal}
	 * 
	 * @param stLiteral
	 * @return
	 */
	public Literal aRTLiteral2RDF4JLiteral(ARTLiteral stLiteral) {
		if (stLiteral == null)
			throw new IllegalArgumentException("resource to be converted must not be null!");
		if (stLiteral == NodeFilters.ANY)
			return null;
		if (stLiteral instanceof ARTLiteralRDF4JImpl)
			return ((ARTLiteralRDF4JImpl) stLiteral).getLiteral();
		if (stLiteral.getDatatype() != null)
			return vFact.createLiteral(stLiteral.getLabel(),
					vFact.createURI(stLiteral.getDatatype().getURI()));
		return vFact.createLiteral(stLiteral.getLabel(), stLiteral.getLanguage());
	}

	// *********************
	// *** RDF4J 2 ART ****
	// *********************

	/**
	 * converts RDF4J {@link Statement} to an OWL ART {@link ARTStatement}
	 * 
	 * @param stat
	 * @return
	 */
	public ARTStatement rdf4jStatement2ARTStatement(Statement stat) {
		if (stat == null)
			return null;
		return new ARTStatementRDF4JImpl(stat);
	}

	/**
	 * converts a RDF4J {@link Resource} to an OWL ART {@link ARTResource}
	 * 
	 * @param res
	 * @return
	 */
	public ARTResource rdf4jResource2ARTResource(Resource res) {
		if (res == null)
			return NodeFilters.ANY;
		else
			return new ARTResourceRDF4JImpl(res);
	}

	/**
	 * converts a RDF4J {@link URI} to an OWL ART {@link ARTURIResource}
	 * 
	 * @param res
	 * @return
	 */
	public ARTURIResource rdf4jURI2ARTURIResource(URI res) {
		if (res == null)
			return NodeFilters.ANY;
		else
			return new ARTURIResourceRDF4JImpl(res);
	}

	/**
	 * converts a RDF4J {@link Value} to an OWL ART {@link ARTNode}
	 * 
	 * @param val
	 * @return
	 */
	public ARTNode rdf4jValue2ARTNode(Value val) {
		if (val == null)
			return NodeFilters.ANY;
		else
			return new ARTNodeRDF4JImpl(val);
	}

	/**
	 * converts a RDF4J {@link Literal} to an OWL ART {@link ARTLiteral}
	 * 
	 * @param sesLiteral
	 * @return
	 */
	public ARTLiteral rdf4jLiteral2ARTLiteral(Literal sesLiteral) {
		if (sesLiteral == null)
			return NodeFilters.ANY;
		else
			return new ARTLiteralRDF4JImpl(sesLiteral);
	}

	/**
	 * converts a RDF4J {@link BNode} to an OWL ART {@link ARTBNode}
	 * 
	 * @param sesBNode
	 * @return
	 */
	public ARTBNode rdf4jBNode2ARTBNode(BNode sesBNode) {
		if (sesBNode == null)
			return NodeFilters.ANY;
		else
			return new ARTBNodeRDF4JImpl(sesBNode);
	}

}
