package it.uniroma2.art.owlart.rdf4jimpl.models;

import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.query.BooleanQuery;
import it.uniroma2.art.owlart.query.GraphQuery;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.Query;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.query.Update;
import it.uniroma2.art.owlart.rdf4jimpl.query.BooleanQueryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.query.GraphQueryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.query.QueryLanguageConverter;
import it.uniroma2.art.owlart.rdf4jimpl.query.TupleQueryRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.query.UpdateRDF4JImpl;

public class TripleQueryModelHTTPConnectioRDF4JImpl implements TripleQueryModelHTTPConnection {

	protected static Logger logger = LoggerFactory.getLogger(TripleQueryModelHTTPConnectioRDF4JImpl.class);

	protected SPARQLRepository localrepository;
	// protected SailConnection conn;
	protected RepositoryConnection repConn;

	public TripleQueryModelHTTPConnectioRDF4JImpl(String endpointURL) throws ModelCreationException {
		localrepository = new SPARQLRepository(endpointURL);
		try {
			localrepository.initialize();
			repConn = localrepository.getConnection();
		} catch (RepositoryException e) {
			throw new ModelCreationException(e);
		}
	}

	public void disconnect() throws ModelAccessException {
		try {
			repConn.close();
		} catch (RepositoryException e) {
			throw new ModelAccessException(e);
		}
	}
	
	
	public Query createQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		try {
			org.eclipse.rdf4j.query.Query ses2query = repConn.prepareQuery(QueryLanguageConverter.convert(ql),
					query, baseURI);
			if (ses2query instanceof org.eclipse.rdf4j.query.TupleQuery) {
				return new TupleQueryRDF4JImpl((org.eclipse.rdf4j.query.TupleQuery) ses2query);
			} else if (ses2query instanceof org.eclipse.rdf4j.query.BooleanQuery)
				return new BooleanQueryRDF4JImpl((org.eclipse.rdf4j.query.BooleanQuery) ses2query);
			else if (ses2query instanceof org.eclipse.rdf4j.query.GraphQuery)
				return new GraphQueryRDF4JImpl((org.eclipse.rdf4j.query.GraphQuery) ses2query);
			else
				throw new ModelAccessException("unknown query type");
		} catch (RepositoryException e) {
			throw new ModelAccessException(e);
		} catch (org.eclipse.rdf4j.query.MalformedQueryException e) {
			logger.error("malformed query");
			throw new MalformedQueryException(e);
		}
	}

	public BooleanQuery createBooleanQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		try {
			return new BooleanQueryRDF4JImpl(repConn.prepareBooleanQuery(
					QueryLanguageConverter.convert(ql), query, baseURI));
		} catch (RepositoryException e) {
			throw new ModelAccessException(e);
		} catch (org.eclipse.rdf4j.query.MalformedQueryException e) {
			throw new MalformedQueryException(e);
		}
	}

	public GraphQuery createGraphQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		try {
			return new GraphQueryRDF4JImpl(repConn.prepareGraphQuery(QueryLanguageConverter.convert(ql),
					query, baseURI));
		} catch (RepositoryException e) {
			throw new ModelAccessException(e);
		} catch (org.eclipse.rdf4j.query.MalformedQueryException e) {
			throw new MalformedQueryException(e);
		}
	}

	public TupleQuery createTupleQuery(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		try {
			return new TupleQueryRDF4JImpl(repConn.prepareTupleQuery(QueryLanguageConverter.convert(ql),
					query, baseURI));
		} catch (RepositoryException e) {
			throw new ModelAccessException(e);
		} catch (org.eclipse.rdf4j.query.MalformedQueryException e) {
			throw new MalformedQueryException(e);
		}
	}

	public Update createUpdate(QueryLanguage ql, String query, String baseURI)
			throws UnsupportedQueryLanguageException, ModelAccessException, MalformedQueryException {
		try {
			return new UpdateRDF4JImpl(repConn.prepareUpdate(QueryLanguageConverter.convert(ql),
					query, baseURI));
		} catch (RepositoryException e) {
			throw new ModelAccessException(e);
		} catch (org.eclipse.rdf4j.query.MalformedQueryException e) {
			throw new MalformedQueryException(e);
		}
	}

}
