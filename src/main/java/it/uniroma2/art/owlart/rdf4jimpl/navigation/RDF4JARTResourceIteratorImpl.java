/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.navigation;

import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.repository.RepositoryException;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.navigation.ARTResourceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTResourceRDF4JImpl;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class RDF4JARTResourceIteratorImpl extends RDFIteratorImpl<ARTResource> implements ARTResourceIterator {
	
	private CloseableIteration<? extends Resource, RepositoryException> resIt;
    
    public RDF4JARTResourceIteratorImpl(CloseableIteration<? extends Resource, RepositoryException> statIt) {
        this.resIt = statIt;
    }

	public void close() throws ModelAccessException {
        try {
            resIt.close();
        } catch (RepositoryException e) {
            throw new ModelAccessException(e);
        }		
	}

	public ARTResource getNext() throws ModelAccessException {
        try {
			return new ARTResourceRDF4JImpl(resIt.next());
        } catch (RepositoryException e) {           
            throw new ModelAccessException(e);
        }
	}

	public boolean streamOpen() throws ModelAccessException {
        try {
			return resIt.hasNext();
		} catch (RepositoryException e) {			
			throw new ModelAccessException(e);
		}
	}
	
	public String toString() {
		return resIt.toString();
	}

}
