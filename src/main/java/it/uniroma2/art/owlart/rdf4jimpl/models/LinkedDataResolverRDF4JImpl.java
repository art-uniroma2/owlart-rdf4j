/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2014.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.models;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.ValueFactoryImpl;
import org.eclipse.rdf4j.repository.util.RDFLoader;
import org.eclipse.rdf4j.rio.ParserConfig;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.rio.helpers.RDFHandlerBase;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.LinkedDataResolver;
import it.uniroma2.art.owlart.rdf4jimpl.RDF4JARTResourceFactory;

/**
 * Implementation of {@link LinkedDataResolver} for RDF4J.
 * 
 * <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 * 
 */
public class LinkedDataResolverRDF4JImpl implements LinkedDataResolver {

	private ValueFactoryImpl valueFactory;
	private ParserConfig parserConfig;
	private RDF4JARTResourceFactory ses2artFact;

	public LinkedDataResolverRDF4JImpl() {
		this.valueFactory = ValueFactoryImpl.getInstance();
		this.ses2artFact = new RDF4JARTResourceFactory(this.valueFactory);
		this.parserConfig = new ParserConfig();
	}

	public ValueFactory getValueFactory() {
		return valueFactory;
	}

	public ParserConfig getParserConfig() {
		return parserConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.LinkedDataResolver#lookup(it.uniroma2.art.owlart.model.ARTURIResource)
	 */
	@Override
	public Collection<ARTStatement> lookup(ARTURIResource resource) throws ModelAccessException,
			MalformedURLException, IOException {
		// Implementation based on org.openrdf.repository.base.RepositoryConnectionBase
		RDFLoader rdfLoader = new RDFLoader(getParserConfig(), getValueFactory());

		ARTContextStatementCollectorRDF4J rdfHandler = new ARTContextStatementCollectorRDF4J();

		try {
			rdfLoader.load(new URL(resource.getNominalValue()), null, null, rdfHandler);
		} catch (RDFParseException e) {
			throw new ModelAccessException(e);
		} catch (RDFHandlerException e) {
			throw new ModelAccessException();
		} catch (UnsupportedRDFormatException e) {
			throw new IOException(e);
		}

		return rdfHandler.getStatements();
	}

	/**
	 * This class is an implementation of {@link RDFHandlerBase} that collects statements into a collection of
	 * {@link ARTStatement}s.
	 */
	private class ARTContextStatementCollectorRDF4J extends RDFHandlerBase {

		private Collection<ARTStatement> statements;

		public ARTContextStatementCollectorRDF4J() {
			this.statements = new HashSet<ARTStatement>();
		}

		@Override
		public void handleStatement(Statement stat) throws RDFHandlerException {

			this.statements.add(ses2artFact.rdf4jStatement2ARTStatement(stat));
		}

		public Collection<ARTStatement> getStatements() {
			return statements;
		}
	}
}
