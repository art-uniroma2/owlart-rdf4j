/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.query;

import java.util.HashMap;

import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.query.QueryLanguage;

public class QueryLanguageConverter {

	private static HashMap<QueryLanguage, org.eclipse.rdf4j.query.QueryLanguage> conversionMap;

	static {
		conversionMap = new HashMap<QueryLanguage, org.eclipse.rdf4j.query.QueryLanguage>();
		conversionMap.put(QueryLanguage.SPARQL, org.eclipse.rdf4j.query.QueryLanguage.SPARQL);
	}

	public static org.eclipse.rdf4j.query.QueryLanguage convert(QueryLanguage ql) throws UnsupportedQueryLanguageException {
		org.eclipse.rdf4j.query.QueryLanguage output = conversionMap.get(ql);
		if (output == null)
			throw new UnsupportedQueryLanguageException("query language: " + ql
					+ " is not supported by current RDF4J OWLArt Implementation");
		else
			return output;
	}


}
