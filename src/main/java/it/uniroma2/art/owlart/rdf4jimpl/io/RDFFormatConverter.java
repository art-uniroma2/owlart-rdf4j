/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.io;

import java.util.HashMap;

import org.eclipse.rdf4j.rio.RDFWriterFactory;

import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;

/**
 * this class provides static conversion methods between OWL ART supported RDF serialization formats and their
 * definitions in RDF4J library
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class RDFFormatConverter {

	private static HashMap<RDFFormat, org.eclipse.rdf4j.rio.RDFFormat> conversionMap;
	private static HashMap<RDFFormat, org.eclipse.rdf4j.rio.RDFWriterFactory> writersMap;

	static {
		conversionMap = new HashMap<RDFFormat, org.eclipse.rdf4j.rio.RDFFormat>();
		conversionMap.put(RDFFormat.N3, org.eclipse.rdf4j.rio.RDFFormat.N3);
		conversionMap.put(RDFFormat.NTRIPLES, org.eclipse.rdf4j.rio.RDFFormat.NTRIPLES);
		conversionMap.put(RDFFormat.RDFXML, org.eclipse.rdf4j.rio.RDFFormat.RDFXML);
		conversionMap.put(RDFFormat.RDFXML_ABBREV, org.eclipse.rdf4j.rio.RDFFormat.RDFXML);
		conversionMap.put(RDFFormat.TRIG, org.eclipse.rdf4j.rio.RDFFormat.TRIG);
		conversionMap.put(RDFFormat.TRIX, org.eclipse.rdf4j.rio.RDFFormat.TRIX);
		conversionMap.put(RDFFormat.TURTLE, org.eclipse.rdf4j.rio.RDFFormat.TURTLE);
		conversionMap.put(RDFFormat.NQUADS, org.eclipse.rdf4j.rio.RDFFormat.NQUADS);
		conversionMap.put(RDFFormat.JSONLD, org.eclipse.rdf4j.rio.RDFFormat.JSONLD);

		writersMap = new HashMap<RDFFormat, RDFWriterFactory>();
		writersMap.put(RDFFormat.N3, new org.eclipse.rdf4j.rio.n3.N3WriterFactory());
		writersMap.put(RDFFormat.NTRIPLES, new org.eclipse.rdf4j.rio.ntriples.NTriplesWriterFactory());
		writersMap.put(RDFFormat.RDFXML, new org.eclipse.rdf4j.rio.rdfxml.RDFXMLWriterFactory());
		writersMap.put(RDFFormat.RDFXML_ABBREV, new org.eclipse.rdf4j.rio.rdfxml.util.RDFXMLPrettyWriterFactory());
		writersMap.put(RDFFormat.TRIG, new org.eclipse.rdf4j.rio.trig.TriGWriterFactory());
		writersMap.put(RDFFormat.TRIX, new org.eclipse.rdf4j.rio.trix.TriXWriterFactory());
		writersMap.put(RDFFormat.TURTLE, new org.eclipse.rdf4j.rio.turtle.TurtleWriterFactory());
		writersMap.put(RDFFormat.NQUADS, new org.eclipse.rdf4j.rio.nquads.NQuadsWriterFactory());
		writersMap.put(RDFFormat.JSONLD, new org.eclipse.rdf4j.rio.jsonld.JSONLDWriterFactory());
	}

	/**
	 * gets the RDF4J RDF Format from its OWL ART counterpart 
	 * 
	 * @param format
	 * @return
	 * @throws UnsupportedRDFFormatException
	 */
	public static org.eclipse.rdf4j.rio.RDFFormat convert(RDFFormat format) throws UnsupportedRDFFormatException {
		org.eclipse.rdf4j.rio.RDFFormat output = conversionMap.get(format);
		if (output == null)
			throw new UnsupportedRDFFormatException("format: " + format
					+ " is not supported by current RDF4J OWLArt Implementation");
		else
			return output;
	}

	/**
	 * gets a RDF4J WriterFactory from OWL ART {@link RDFFormat}
	 * 
	 * @param format
	 * @return
	 * @throws UnsupportedRDFFormatException
	 */
	public static RDFWriterFactory getWriterFactory(RDFFormat format) throws UnsupportedRDFFormatException {
		RDFWriterFactory wFact = writersMap.get(format);
		if (wFact == null)
			throw new UnsupportedRDFFormatException("format: " + format
					+ " is not supported by current RDF4J OWLArt Implementation");
		else
			return wFact;
	}

}
