package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;

public class RDF4JPersistentInMemoryModelConfiguration extends RDF4JDirectAccessModelConfiguration
		implements PersistenceModelConfiguration, RDF4JInMemoryModelConfiguration {

	@ModelConfigurationParameter(description = "time in milliseconds before model is persisted; default is 1000 ms")
	public long syncDelay = 1000L;

	public RDF4JPersistentInMemoryModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "in memory / persistent";
	}

	public boolean isPersistent() {
		return true;
	}

}
