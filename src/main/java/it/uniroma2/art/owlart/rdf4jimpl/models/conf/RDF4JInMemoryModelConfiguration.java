package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

import org.eclipse.rdf4j.sail.memory.MemoryStore;

import it.uniroma2.art.owlart.rdf4jimpl.factory.ARTModelFactoryRDF4JImpl;

/**
 * This interface has no methods, it is just a tag to inform the {@link ARTModelFactoryRDF4JImpl} that a the
 * repository which will be created is based on the RDF4J {@link MemoryStore} sail implementation
 * 
 * @author Armando Stellato <a href="mailto:stellato@info.uniroma2.it">stellato@info.uniroma2.it</a>
 * 
 */
public interface RDF4JInMemoryModelConfiguration extends RDF4JModelConfiguration {

}
