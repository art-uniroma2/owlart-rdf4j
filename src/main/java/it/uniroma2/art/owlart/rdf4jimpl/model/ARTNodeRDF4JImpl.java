/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.model;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.URI;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.owlart.model.ARTBNode;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 *
 */
public class ARTNodeRDF4JImpl implements ARTNode {

	Value node;

    public ARTNodeRDF4JImpl(Value res) {
        this.node = res; 
    }
    
    /**
     * convenience method for extracting encapsulated RDF4J {@link Value}
     * 
     * @return
     */
    public Value getRDF4JValue() {
    	return node;
    }
    
	public ARTURIResource asURIResource() {
		return new ARTURIResourceRDF4JImpl((URI)node);
	}
	
	public ARTBNode asBNode() {
		return new ARTBNodeRDF4JImpl((BNode)node);
	}
    
	public ARTResource asResource() {
		return new ARTResourceRDF4JImpl((Resource)node);
	}

	public ARTLiteral asLiteral() {
		return new ARTLiteralRDF4JImpl((Literal)node);
	}

	public boolean isBlank() {
		return (node instanceof BNode);
	}

	public boolean isLiteral() {
		return (node instanceof Literal);
	}

	public boolean isURIResource() {
		return (node instanceof URI);
	}

	public boolean isResource() {
		return (node instanceof Resource);
	}
    
	public String getNominalValue() {
		return node.stringValue();
	}
	
	public String toString() {
		return node.toString();
	}
	
    public boolean equals(Object o) {
    	
    	// System.err.println("inside equals of empty URI resource between: " + this + " and " + o);
    	
        if (this == o) {
            return true;
        }

        if (o instanceof ARTNode) {
            return toString().equals(o.toString());
        }
        
        return false;
    }
	
	public int hashCode() {
    	return node.hashCode();
    }
}
