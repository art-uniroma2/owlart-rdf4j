package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationImpl;
import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;

public abstract class RDF4JDirectAccessModelConfiguration extends ModelConfigurationImpl implements
		RDF4JModelConfiguration {

	@ModelConfigurationParameter(description = "true if the RDF4J repository has to support directType inference; defaults to true")
	public boolean directTypeInference = true;

	@ModelConfigurationParameter(description = "true if the RDF4J repository has to support RDFS inferencing; defaults to true")
	public boolean rdfsInference = true;

	public RDF4JDirectAccessModelConfiguration() {
		super();
	}

}
