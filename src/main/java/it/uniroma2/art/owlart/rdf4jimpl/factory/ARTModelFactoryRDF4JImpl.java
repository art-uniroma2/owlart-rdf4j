/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.factory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.config.RepositoryConfigException;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.SailException;
import org.eclipse.rdf4j.sail.helpers.AbstractNotifyingSail;
import org.eclipse.rdf4j.sail.inferencer.fc.DirectTypeHierarchyInferencer;
import org.eclipse.rdf4j.sail.inferencer.fc.ForwardChainingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.LinkedDataResolver;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.BaseRDFModelRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.LinkedDataResolverRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.OWLModelRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.RDFModelRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.RDFSModelRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.SKOSModelRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.SKOSXLModelRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.TripleQueryModelHTTPConnectioRDF4JImpl;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JDirectAccessModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JInMemoryModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JNativeModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JNonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JPersistentInMemoryModelConfiguration;
import it.uniroma2.art.owlart.rdf4jimpl.models.conf.RDF4JRemoteModelConfiguration;

/**
 * This class implements {@link ModelFactory}<br/>
 * 
 * A fast and easy way to load an RDF/RDFS/OWL Model by using this RDF4J implementation is to wrap it
 * through the OWL API convenience class {@link OWLArtModelFactory}, which handles standard configuration of
 * models of the RDF family, like loading proper vocabularies, setting baseuri/defnamespace of the loaded
 * model etc... <br/>
 * This is the code to do that:
 * 
 * <pre>
 * ModelFactory fact = OWLArtModelFactory.createModelFactory(&lt;an instance of this class&gt;);
 * </pre>
 * 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public class ARTModelFactoryRDF4JImpl implements ModelFactory<RDF4JModelConfiguration> {

	protected static Logger logger = LoggerFactory.getLogger(ARTModelFactoryRDF4JImpl.class);

	ArrayList<Class<? extends RDF4JModelConfiguration>> supportedConfigurationClasses;

	protected boolean populatingW3CVocabularies = true;

	RDF4JNonPersistentInMemoryModelConfiguration quickConf;

	public ARTModelFactoryRDF4JImpl() {
		supportedConfigurationClasses = new ArrayList<Class<? extends RDF4JModelConfiguration>>();
		supportedConfigurationClasses.add(RDF4JPersistentInMemoryModelConfiguration.class);
		supportedConfigurationClasses.add(RDF4JNonPersistentInMemoryModelConfiguration.class);
		supportedConfigurationClasses.add(RDF4JNativeModelConfiguration.class);
		supportedConfigurationClasses.add(RDF4JRemoteModelConfiguration.class);

		// this is preloaded, to avoid recreating it each time a new quick model is being constructed
		try {
			quickConf = this
					.createModelConfigurationObject(RDF4JNonPersistentInMemoryModelConfiguration.class);
			quickConf.directTypeInference = false;
			quickConf.rdfsInference = false;
		} catch (UnsupportedModelConfigurationException | UnloadableModelConfigurationException e) {
			throw new IllegalStateException(
					"inconsistent configuration exception; this is not depending on misuse, and should never happen; something is broken in this version of RDF4JImpl");
		}

//		/*
//		 * Unregisters the JSON RDF Parser, because it is compatible with dbpedia.org (2014-06-13). TODO: find
//		 * a better approach, because the following code is vulnerable to race conditions, if sesame2 is
//		 * accessed directly.
//		 */
//		synchronized (RDFParserRegistry.class) {
//			RDFParserRegistry defaultParserRegistry = RDFParserRegistry.getInstance();
//
//			RDFParserFactory rdfJSONParserFactory = defaultParserRegistry.get(RDFFormat.RDFJSON);
//
//			if (rdfJSONParserFactory != null) {
//				defaultParserRegistry.remove(rdfJSONParserFactory);
//			}
//
//		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.uniroma2.art.owlart.models.ModelFactory#closeModel(it.uniroma2.art.owlart.models.BaseRDFTripleModel)
	 */
	public void closeModel(BaseRDFTripleModel rep) throws ModelUpdateException {
		rep.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFBaseModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public BaseRDFModelRDF4JImpl loadRDFBaseModel(String baseuri, String repositoryDirectory,
			RDF4JModelConfiguration conf) throws ModelCreationException {

		File dataDir = null;
		if (repositoryDirectory != null){
			dataDir = new File(repositoryDirectory);
			if (!dataDir.exists())
				throw new ModelCreationException("you must specify an existig directory for this repository; "
						+ dataDir + " does not exist");
		}
		if (repositoryDirectory == null && isPersistentConf(conf)){
			throw new ModelCreationException("Inconsistent configuration: "
					+ "null repository directory provided for a persistent configuration");
		}

		AbstractNotifyingSail baseSail;
		// SailRepository myRepository;
		Repository myRepository;
		boolean rdfsReasoning = false;
		boolean directTypeReasoning = false;
		boolean inferredNullContextContainsAllTriples = true;

		logger.info("creating RDF4J Model");

		if (conf instanceof RDF4JDirectAccessModelConfiguration) {

			if (conf instanceof RDF4JInMemoryModelConfiguration) {
				baseSail = new MemoryStore(dataDir);
				if (conf instanceof RDF4JPersistentInMemoryModelConfiguration) {
					((MemoryStore) baseSail).setPersist(true);
					long syncDelay = ((RDF4JPersistentInMemoryModelConfiguration) conf).syncDelay;
					logger.info("syncDelay: " + syncDelay);
					((MemoryStore) baseSail).setSyncDelay(syncDelay);
				} else
					((MemoryStore) baseSail).setPersist(false);

			} else if (conf instanceof RDF4JNativeModelConfiguration) {
				baseSail = new NativeStore(dataDir);
				((NativeStore) baseSail).setForceSync(((RDF4JNativeModelConfiguration) conf).forceSync);
				((NativeStore) baseSail)
						.setTripleIndexes(((RDF4JNativeModelConfiguration) conf).tripleIndexes);

			} else
				throw new ModelCreationException("unknown configuration type: " + conf.getClass());

			RDF4JDirectAccessModelConfiguration daconf = (RDF4JDirectAccessModelConfiguration) conf;

			if (!daconf.directTypeInference && !daconf.rdfsInference)
				myRepository = new SailRepository(baseSail);
			else if (daconf.directTypeInference && !daconf.rdfsInference)
				myRepository = new SailRepository(new DirectTypeHierarchyInferencer(baseSail));
			else if (!daconf.directTypeInference && daconf.rdfsInference)
				myRepository = new SailRepository(new ForwardChainingRDFSInferencer(baseSail));
			else
				// both of them active
				myRepository = new SailRepository(new DirectTypeHierarchyInferencer(
						new ForwardChainingRDFSInferencer(baseSail)));

			rdfsReasoning = daconf.rdfsInference;
			directTypeReasoning = daconf.directTypeInference;

		} else { // remote access

			RDF4JRemoteModelConfiguration remModelConf = (RDF4JRemoteModelConfiguration) conf;

			try {
				RemoteRepositoryManager repositoryManager = RemoteRepositoryManager.getInstance(
						remModelConf.serverURL, remModelConf.username, remModelConf.password);
				myRepository = repositoryManager.getRepository(remModelConf.repositoryId);
				if (myRepository == null)
					throw new ModelCreationException("there is no repository with id: "
							+ remModelConf.repositoryId);

			} catch (RepositoryException e) {
				throw new ModelCreationException("Problem with remote connection: " + e.getMessage());
			} catch (RepositoryConfigException e) {
				throw new ModelCreationException("Problem with remote connection: " + e.getMessage());
			}

			rdfsReasoning = true;
			directTypeReasoning = true;
			inferredNullContextContainsAllTriples = remModelConf.inferredNullContextContainsAllTriples;
			// TODO
			// ricordati poi, qualora fosse possibile appurare dal server se un repository è configurato con
			// rdfs reasoning/direct type o meno (OWLIM dovrebbe avere di serie un sottoinsieme di OWL, ma
			// forse va configurato dal server, e forse, dico forse, il client può, al momento della
			// connessione o cmq con una query successiva, sapere che livello di reasoning ha.
			// io per ora metto true perchè immagino che OWLIM abbia rdfs reasoning almeno.

		}

		try {
			myRepository.initialize();
		} catch (RepositoryException e) {
			throw new ModelCreationException(e.getMessage());
		}

		logger.info("RDF4J RDF Model initialization...");
		try {
			BaseRDFModelRDF4JImpl rep = new BaseRDFModelRDF4JImpl(myRepository, rdfsReasoning,
					directTypeReasoning);
			rep.setInferredNullContextContainsAllTriples(inferredNullContextContainsAllTriples);
			return rep;
		} catch (SailException e) {
			throw new ModelCreationException(e.getMessage());
		} catch (RepositoryException e) {
			throw new ModelCreationException(e.getMessage());
		}
	}

	public BaseRDFTripleModel createLightweightRDFModel() {
		try {
			return this.loadRDFBaseModel(null, null, quickConf);
		} catch (ModelCreationException e) {
			throw new IllegalStateException(
					"inconsistent configuration exception; this is not depending on misuse, and should never happen; something is broken in this version of RDF4JImpl");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public RDFModel loadRDFModel(String baseuri, String repositoryDirectory, RDF4JModelConfiguration conf)
			throws ModelCreationException {
		BaseRDFModelRDF4JImpl baserep = loadRDFBaseModel(baseuri, repositoryDirectory, conf);
		try {
			RDFModelRDF4JImpl rep = new RDFModelRDF4JImpl(baserep);
			return rep;
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadRDFSModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public RDFSModel loadRDFSModel(String baseuri, String repositoryDirectory, RDF4JModelConfiguration conf)
			throws ModelCreationException {
		BaseRDFModelRDF4JImpl baserep = loadRDFBaseModel(baseuri, repositoryDirectory, conf);
		try {
			RDFSModelRDF4JImpl rep = new RDFSModelRDF4JImpl(baserep);
			return rep;
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadOWLModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public OWLModel loadOWLModel(String baseuri, String repositoryDirectory, RDF4JModelConfiguration conf)
			throws ModelCreationException {
		BaseRDFModelRDF4JImpl baserep = loadRDFBaseModel(baseuri, repositoryDirectory, conf);
		try {
			OWLModelRDF4JImpl rep = new OWLModelRDF4JImpl(baserep);

			// ARTURIResource owl = rep.createURIResource(OWL.NAMESPACE);
			// logger.debug("owl namespace: " + owl.getURI());
			// rep.addRDF(Resources.class.getResource("owl.rdfs"), OWL.NAMESPACE, RDFFormat.RDFXML, owl);

			return rep;
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadSKOSModel(java.lang.String, java.lang.String,
	 * boolean)
	 */
	public SKOSModel loadSKOSModel(String baseuri, String persistenceDirectory, RDF4JModelConfiguration conf)
			throws ModelCreationException {
		BaseRDFModelRDF4JImpl baserep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);
		try {
			SKOSModelRDF4JImpl rep = new SKOSModelRDF4JImpl(baserep);

			// ARTURIResource skos = rep.createURIResource(SKOS.NAMESPACE);
			// ARTURIResource owl = rep.createURIResource(OWL.NAMESPACE);
			// rep.addRDF(Resources.class.getResource("owl.rdfs"), OWL.NAMESPACE, RDFFormat.RDFXML, owl);
			// rep.addRDF(Resources.class.getResource("skos.rdf"), SKOS.NAMESPACE, RDFFormat.RDFXML, skos);

			return rep;
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	public SKOSXLModel loadSKOSXLModel(String baseuri, String persistenceDirectory,
			RDF4JModelConfiguration conf) throws ModelCreationException {
		BaseRDFModelRDF4JImpl baserep = loadRDFBaseModel(baseuri, persistenceDirectory, conf);
		try {
			SKOSXLModelRDF4JImpl rep = new SKOSXLModelRDF4JImpl(baserep);

			// ARTURIResource owl = rep.createURIResource(OWL.NAMESPACE);
			// ARTURIResource skos = rep.createURIResource(SKOS.NAMESPACE);
			// ARTURIResource skosxl = rep.createURIResource(SKOSXL.NAMESPACE);
			// logger.debug("owl namespace: " + owl.getURI());
			// logger.debug("skos namespace: " + skos.getURI());
			// logger.debug("skos xl namespace: " + skosxl.getURI());
			// rep.addRDF(Resources.class.getResource("owl.rdfs"), OWL.NAMESPACE, RDFFormat.RDFXML, owl);
			// rep.addRDF(Resources.class.getResource("skos.rdf"), SKOS.NAMESPACE, RDFFormat.RDFXML, skos);
			// rep.addRDF(Resources.class.getResource("skos-xl.rdf"), SKOSXL.NAMESPACE, RDFFormat.RDFXML,
			// skosxl);

			return rep;
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}
	}

	public Collection<Class<? extends RDF4JModelConfiguration>> getModelConfigurations() {
		return supportedConfigurationClasses;
	}

	public <MCImpl extends RDF4JModelConfiguration> MCImpl createModelConfigurationObject(
			Class<MCImpl> mcclass) throws UnsupportedModelConfigurationException,
			UnloadableModelConfigurationException {
		logger.debug("creating ModelConfigurationObject");
		if (supportedConfigurationClasses.contains(mcclass)) {
			// if (true) {
			try {

				// logger.debug("class loader of: " + this.getClass().getSimpleName() + " instance: " +
				// this.getClass().getClassLoader());
				// logger.debug("class loader of static: " + ARTModelFactorySesame2Impl.class.getSimpleName()
				// + " : " + ARTModelFactorySesame2Impl.class.getClassLoader());
				// logger.debug("class loader of passed model configuration class: " + mcclass.getSimpleName()
				// + "inside: " + this.getClass().getSimpleName() + " = " + mcclass.getClassLoader());
				// logger.debug("class loader of inner model configuration class: " +
				// innerMConfCls.getSimpleName() + "inside: " + this.getClass().getSimpleName() + " = " +
				// innerMConfCls.getClassLoader());

				logger.debug("requested model configuration class: " + mcclass);
				// logger.debug("inner model configuration class: " + innerMConfCls );

				MCImpl mConf = (MCImpl) mcclass.newInstance();

				// MCImpl mConf = mcclass.newInstance();

				// logger.debug("mConf: " + mConf );
				// return mcclass.newInstance();
				return mConf;
			} catch (InstantiationException e) {
				throw new UnloadableModelConfigurationException(mcclass);
			} catch (IllegalAccessException e) {
				throw new UnloadableModelConfigurationException(mcclass);
			}
		} else
			throw new UnsupportedModelConfigurationException(this, mcclass);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadTripleQueryHTTPConnection(java.lang.String)
	 */
	public TripleQueryModelHTTPConnection loadTripleQueryHTTPConnection(String endpointURL)
			throws ModelCreationException {
		return new TripleQueryModelHTTPConnectioRDF4JImpl(endpointURL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.owlart.models.ModelFactory#loadLinkedDataResolver()
	 */
	public LinkedDataResolver loadLinkedDataResolver() {
		return new LinkedDataResolverRDF4JImpl();
	}

	public void setPopulatingW3CVocabularies(boolean pref) {
		populatingW3CVocabularies = pref;
	}

	public boolean isPopulatingW3CVocabularies() {
		return populatingW3CVocabularies;
	}
	
	private boolean isPersistentConf(ModelConfiguration conf){
		return !(conf instanceof RDF4JNonPersistentInMemoryModelConfiguration);
	}

}
