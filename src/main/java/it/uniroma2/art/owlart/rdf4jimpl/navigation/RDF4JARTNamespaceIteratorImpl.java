/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.navigation;

import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.repository.RepositoryException;

import it.uniroma2.art.owlart.model.ARTNamespace;
import it.uniroma2.art.owlart.navigation.ARTNamespaceIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTNamespaceRDF4JImpl;

public class RDF4JARTNamespaceIteratorImpl extends RDFIteratorImpl<ARTNamespace> implements ARTNamespaceIterator {
	
	private CloseableIteration<Namespace, RepositoryException> nsit;
	
	public RDF4JARTNamespaceIteratorImpl(CloseableIteration<Namespace, RepositoryException> nsit) {
		this.nsit = (CloseableIteration<Namespace, RepositoryException>) nsit;
	}
	
	public boolean streamOpen() throws it.uniroma2.art.owlart.exceptions.ModelAccessException {
		try {
			return nsit.hasNext();
		} catch (RepositoryException e) {
		    throw new it.uniroma2.art.owlart.exceptions.ModelAccessException(e);
		}
	}

	public ARTNamespace getNext() throws it.uniroma2.art.owlart.exceptions.ModelAccessException {
		try {
			return new ARTNamespaceRDF4JImpl(nsit.next());
		} catch (RepositoryException e) {
		    throw new it.uniroma2.art.owlart.exceptions.ModelAccessException(e);
		}
	}

    public void close() throws it.uniroma2.art.owlart.exceptions.ModelAccessException {
        try {
            nsit.close();
        } catch (RepositoryException e) {
            throw new it.uniroma2.art.owlart.exceptions.ModelAccessException(e);
        }
    }

}
