/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.models;


import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.TransactionBasedModel;
import it.uniroma2.art.owlart.models.impl.RDFModelImpl;
import it.uniroma2.art.owlart.rdf4jimpl.vocabulary.SESAME;

public class RDFModelRDF4JImpl extends RDFModelImpl implements RDFModelRDF4J, TransactionBasedModel {

	public RDFModelRDF4JImpl(BaseRDFModelRDF4JImpl baseRep) throws VocabularyInitializationException {
		super(baseRep);
        SESAME.Res.initialize(this);
	}

	public void commit() throws ModelUpdateException {
		((TransactionBasedModel)baseRep).commit();		
	}

	public boolean isAutoCommit() throws ModelAccessException {
		return ((TransactionBasedModel)baseRep).isAutoCommit();
	}

	public void rollBack() throws ModelAccessException {
		((TransactionBasedModel)baseRep).rollBack();		
	}

	public void setAutoCommit(boolean value) throws ModelUpdateException {
		((TransactionBasedModel)baseRep).setAutoCommit(value);
	}
	
	public RepositoryConnection getRDF4JRepositoryConnection() {
		return ((BaseRDFModelRDF4JImpl)baseRep).getRDF4JRepositoryConnection();
	}
	
	public Repository getRDF4JRepository() {
		return ((BaseRDFModelRDF4JImpl) baseRep).getRDF4JRepository();
	}
	
	/****************************
	 *    FORKING OPERATIONS    *
	 ****************************/

	@Override
	public RDFModelRDF4JImpl forkModel() throws ModelCreationException {
		if (this.getClass() != RDFModelRDF4JImpl.class) {
			throw new IllegalStateException("The model class '" + this.getClass().getName() + "' seems not properly override forkModel()");
		}
		try {
			return new RDFModelRDF4JImpl((BaseRDFModelRDF4JImpl)baseRep.forkModel());
		} catch (VocabularyInitializationException e) {
			throw new ModelCreationException(e);
		}	
	}


}
