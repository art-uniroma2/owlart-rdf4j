/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.navigation;

import org.eclipse.rdf4j.common.iteration.CloseableIteration;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.rdf4jimpl.model.ARTStatementRDF4JImpl;

import org.eclipse.rdf4j.OpenRDFException;
import org.eclipse.rdf4j.model.Statement;

/**
 * @author Giuseppe Castellucci
 *
 */
public class RDF4JARTStatementIteratorImpl extends RDFIteratorImpl<ARTStatement> implements ARTStatementIterator {
	
	private CloseableIteration<? extends Statement, ? extends OpenRDFException> statIt;
    
    public RDF4JARTStatementIteratorImpl(CloseableIteration<? extends Statement, ? extends OpenRDFException> statIt) {
        this.statIt = statIt;
    }

	public void close() throws ModelAccessException {
        try {
            statIt.close();
        } catch (OpenRDFException e) {
            throw new ModelAccessException(e);
        }		
	}

	public ARTStatement getNext() throws ModelAccessException {
        try {
			return new ARTStatementRDF4JImpl(statIt.next());
        } catch (OpenRDFException e) {           
            throw new ModelAccessException(e);
        }
	}

	public boolean streamOpen() throws ModelAccessException {
        try {
			return statIt.hasNext();
		} catch (OpenRDFException e) {			
			throw new ModelAccessException(e);
		}
	}
	
	public String toString() {
		return statIt.toString();
	}

}
