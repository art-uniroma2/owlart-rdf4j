/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDF4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDF4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDF4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.model;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.URI;

import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;

/**
 * @author Armando Stellato <stellato@info.uniroma2.it>
 * 
 */
public class ARTStatementRDF4JImpl implements ARTStatement {

	Statement statement;

	public ARTStatementRDF4JImpl(Statement stm) {
		this.statement = stm;
	}

	public ARTNode getObject() {
		return new ARTNodeRDF4JImpl(statement.getObject());
	}

	public ARTURIResource getPredicate() {
		return new ARTURIResourceRDF4JImpl(statement.getPredicate());
	}

	public ARTResource getSubject() {
		return new ARTResourceRDF4JImpl(statement.getSubject());
	}

	/**
	 * convenience method for extracting encapsulated RDF4J {@link BNode}
	 * 
	 * @return
	 */
	public Statement getRDF4JStatement() {
		return statement;
	}

	public String toString() {
		return statement.toString();
	}

	public ARTResource getNamedGraph() {
		URI context = (URI) statement.getContext();
		if (context == null)
			return NodeFilters.MAINGRAPH;
		else
			return new ARTURIResourceRDF4JImpl(context);
	}

	public boolean equals(Object o) {
    	    	
        if (this == o) {
            return true;
        }

        if (o instanceof ARTStatement) {
        	ARTStatement otherStat = (ARTStatement)o;
            return ( 
            		(this.getSubject().equals(otherStat.getSubject())) && 
            		(this.getPredicate().equals(otherStat.getPredicate())) && 
            		(this.getObject().equals(otherStat.getObject()))
            );
        }
        
        return false;
    }
	
	public int hashCode() {
		return 961 * getSubject().hashCode() + 31 * getPredicate().hashCode() + getObject().hashCode();	
	}
	
}
