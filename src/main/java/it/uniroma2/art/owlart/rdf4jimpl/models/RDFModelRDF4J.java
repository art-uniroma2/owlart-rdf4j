package it.uniroma2.art.owlart.rdf4jimpl.models;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;

public interface RDFModelRDF4J {

	public RepositoryConnection getRDF4JRepositoryConnection();
	public Repository getRDF4JRepository();

}
