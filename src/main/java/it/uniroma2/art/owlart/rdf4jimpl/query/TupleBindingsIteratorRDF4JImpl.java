/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is ART Ontology API (RDf4J Implementation).
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2007.
 * All Rights Reserved.
 *
 * ART Ontology API (RDf4J Implementation) was developed by the Artificial Intelligence Research Group
 * (art.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about the ART Ontology API (RDf4J Implementation) can be obtained at 
 * http//art.uniroma2.it/owlart
 *
 */
package it.uniroma2.art.owlart.rdf4jimpl.query;

import java.util.List;

import org.eclipse.rdf4j.query.QueryEvaluationException;
import org.eclipse.rdf4j.query.TupleQueryResult;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.navigation.RDFIteratorImpl;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;

public class TupleBindingsIteratorRDF4JImpl extends RDFIteratorImpl<TupleBindings> implements TupleBindingsIterator {

	private TupleQueryResult statIt;
    
    public TupleBindingsIteratorRDF4JImpl(TupleQueryResult statIt) {
        this.statIt = statIt;
    }
	
	public List<String> getBindingNames() throws ModelAccessException {
		try {
			return statIt.getBindingNames();
		} catch (QueryEvaluationException e) {
			throw new ModelAccessException(e);
		}		
	}

	public void close() throws ModelAccessException {
		try {
			statIt.close();
		} catch (QueryEvaluationException e) {
			throw new ModelAccessException(e);
		}		
	}

	public TupleBindings getNext() throws ModelAccessException {
		try {
			return new TupleBindingsRDF4JImpl(statIt.next());
		} catch (QueryEvaluationException e) {
			throw new ModelAccessException();
		}
	}

	public boolean streamOpen() throws ModelAccessException {
		try {
			return statIt.hasNext();
		} catch (QueryEvaluationException e) {
			throw new ModelAccessException();
		}
	}


}
