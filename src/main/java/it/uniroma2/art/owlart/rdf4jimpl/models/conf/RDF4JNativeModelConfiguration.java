package it.uniroma2.art.owlart.rdf4jimpl.models.conf;

import it.uniroma2.art.owlart.models.conf.ModelConfigurationParameter;
import it.uniroma2.art.owlart.models.conf.PersistenceModelConfiguration;

public class RDF4JNativeModelConfiguration extends RDF4JDirectAccessModelConfiguration implements
		RDF4JModelConfiguration, PersistenceModelConfiguration {

	@ModelConfigurationParameter(description = "Specifies whether updates should be synced to disk forcefully; defaults to false")
	public Boolean forceSync = false;

	@ModelConfigurationParameter(description = "specifies the triple indexes to be created for optimizing"
			+ "query resolution; see: http://rdf4j.org/doc/programming-with-rdf4j/the-repository-api/#Creating_a_Native_RDF_Repository; defaults to spoc, posc")
	public String tripleIndexes = "spoc, posc";

	public RDF4JNativeModelConfiguration() {
		super();
	}

	public String getShortName() {
		return "native store / persistent";
	}

	public boolean isPersistent() {
		return true;
	}
}
